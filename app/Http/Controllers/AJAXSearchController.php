<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AJAXSearchController extends Controller
{
    
    public function ajaxSearch(Request $request) {
        if($request->ajax()) {
            $result = youtube_search_video($request->search);
            return Response($result);
        }
        return Response("bye");
    }
    
}
