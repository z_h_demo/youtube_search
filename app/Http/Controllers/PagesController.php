<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;

class PagesController extends Controller
{
    public function home()
    {
        return view('welcome');
    }

    public function search()
    {
        return view('search');
    }
    
    public function result() {
        $search = request('search');
        $result = youtube_search_video($search);
        return view('/result')->with(['result'=>$result]);
    }       
}
