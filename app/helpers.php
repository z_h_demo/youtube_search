<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (! function_exists('youtube_search_video')) {
    function youtube_search_video($search) {

        if (trim($search) === '') return [0=>'nothing to search'];
        
        $videoListObjects = (array)Youtube::searchVideos($search);
        $videoListArray = json_decode(json_encode($videoListObjects), true);
        $videoListArray = array_filter($videoListArray,function($result) {
            $ok = 
                    isset($result['kind']) &&
                    $result['kind'] === 'youtube#searchResult' &&
                    isset($result['id']['kind']) &&
                    $result['id']['kind'] === 'youtube#video' &&
                    isset($result['id']['videoId']);
            return $ok;            
        });
        
        if (count($videoListArray) === 0) return [0=>'nothing found'];
        
        $videoIds=array_map(function($video){
            return $video['id']['videoId'];            
        },$videoListArray);
        $videoTitles=array_map(function($id){
            $videoInfo = Youtube::getVideoInfo($id);
            $title='';
            if (property_exists($videoInfo,'snippet') && isset($videoInfo->snippet)) {
                $snippet = $videoInfo->snippet;
                if (property_exists($snippet,'title') && isset($snippet->title)) {                
                    $title = $snippet->title;
                }
            }
            return $title;            
        },$videoIds);
        $result = array_combine($videoIds,$videoTitles);
        return $result;
    }
}