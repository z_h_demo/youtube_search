@extends('layout')

@section('title','Youtube search')

@section('content')
<div>
    <form method="POST" action="/result">
        {{ csrf_field() }}
        <span>
            <input type="text" name="search" placeholder="e.g.: foo">
        </span>
        <span>
            <button type="submit">Search Video
        </span>
    </form>
</div>

<div>
  <div>
    <button type="button" onclick="ajaxSearch()">AJAX Search Video</button>
  </div>
  <div>    
      <textarea id='search_result' rows="12" cols="120"></textarea>
  </div>
</div>

<script>
function ajaxSearch() {
    var search=document.getElementsByName('search')[0].value;
    var responseText;
    var response;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            responseText=this.responseText;
            response = JSON.parse(responseText);
            responseText='';
            for (var prop in response) {
                if (!response.hasOwnProperty(prop)) continue;
                //console.log(prop + " = " + response[prop]);
                responseText += response[prop]+'\n';
            }
            document.getElementById("search_result").innerHTML = responseText;
        }
    };
    //xhttp.open("GET", 'robots.txt', true);
    xhttp.open("GET", '{{URL::to('ajax_search')}}?search='+search, true);
    xhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhttp.setRequestHeader('csrftoken', '{{ csrf_token() }}');
    xhttp.send();
}
</script>
@endsection